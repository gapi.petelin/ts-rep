#!/bin/bash

while true; do
    loadavg=$(uptime | awk -F'[a-z]:' '{print $2}' | awk -F',' '{print $1}' | tr -d ' ')
    currentjobs=$(tsp -S)
    echo "Load average: $loadavg with $currentjobs jobs"

    if [ "$(echo "$loadavg < 30" | bc -l)" -eq 1 ]; then
        echo "Set jobs + 1"
        newjob=$((currentjobs + 1))
        tsp -S $newjob
        sleep 360
    elif [ "$(echo "$loadavg > 60" | bc -l)" -eq 1 ]; then
        echo "Set jobs - 1"
        newjob=$((currentjobs - 1))
        tsp -S $newjob
        sleep 10
    else
        echo "No change"
        sleep 10
    fi

    
done