from sklearn.metrics import accuracy_score
from datetime import datetime
import argparse
import os

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--dataset', type=str, required=True)
    parser.add_argument('--method', type=str, required=True)
    parser.add_argument('--transform', type=str, required=True)
    parser.add_argument('--seed', type=int, default=0)

    args = parser.parse_args()
    print(args)
    
    folder = 'sensitivity'
    file = f'{folder}/{args.dataset}__{args.method}__{args.transform}__{args.seed}.csv'
    if os.path.exists(file):
        print(f'Files {file} already exist.')
    else:
        print('Computing...')
        from utils import *
        X_train, X_test, y_train, y_test = read_dataset(args.dataset)
        X_train, X_test = interpolate_scale(X_train, X_test)
        print('Data loaded')
        clf = trained_classifiers_factory(args.dataset, args.method, args.seed)
        print('Model trained')
        transformation = transformation_factory(args.transform)
        X_test_t = transformation(X_test.copy())
        print('Transformed')
        pred = clf.predict(X_test_t)
        print('Prediction made')
        acc = accuracy_score(y_test, pred)

        data = {'dataset': [], 'method': [], 'transform': [], 'seed':[], 'accuracy':[]}
        data['dataset'].append(args.dataset)
        data['method'].append(args.method)        
        data['transform'].append(args.transform)
        data['seed'].append(args.seed)
        data['accuracy'].append(acc)    
        create_directory_if_not_exist(folder)
        df = pd.DataFrame(data).to_csv(file, index=False)
    
    