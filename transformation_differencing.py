from sklearn.metrics import accuracy_score
from datetime import datetime
import argparse
import os

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--dataset', type=str, required=True)
    parser.add_argument('--method', type=str, required=True)
    parser.add_argument('--transform', type=str, required=True)
    parser.add_argument('--seed', type=int, default=0)
    
    args = parser.parse_args()
    print(args)
    
    folder = 'difference'
    file = f'{folder}/{args.dataset}__{args.method}__{args.transform}__{args.seed}.csv'
    if os.path.exists(file):
        print(f'Files {file} already exist.')
    else:
        print('Computing...')
        from utils import *
        size = 20
        X_train, X_test, y_train, y_test = read_dataset(args.dataset)
        X_train, X_test = interpolate_scale(X_train, X_test)
        X_train, X_test = X_train.sample(n=size, replace=True), X_test.sample(n=size, replace=True)
        
        transformation = transformation_factory(args.transform)
        X_train_transformed = transformation(X_train.copy())
        X_test_transformed = transformation(X_test.copy())
        
        clf = classifiers_factory(args.method, args.seed)
        clf.fit(pd.concat([X_train, X_train_transformed], axis=0).reset_index(drop=True), np.array(['o']*len(X_train)+['t']*len(X_train_transformed)))
        y_pred = clf.predict(pd.concat([X_test, X_test_transformed], axis=0).reset_index(drop=True))
        acc = accuracy_score(['o']*len(X_test)+['t']*len(X_test_transformed), y_pred)
        
        data = {'dataset': [], 'method': [], 'transform': [], 'seed':[], 'accuracy':[]}
        data['dataset'].append(args.dataset)
        data['method'].append(args.method)        
        data['transform'].append(args.transform)
        data['seed'].append(args.seed)
        data['accuracy'].append(acc)    
        create_directory_if_not_exist(folder)
        df = pd.DataFrame(data).to_csv(file, index=False)
        
        
        
        
#if __name__ == "__main__":
#    all_datasets = ['Car', 'Coffee', 'Meat', 'Plane', 'Wine']
#
#    subsample = 20
#    transform = 'WhiteNoise'
#    for dataset in all_datasets:
#        X_train, X_test, _, _ = read_dataset(dataset)
#        X_train_, X_test_ = X_train.sample(subsample), X_test.sample(subsample)
#
#        data = {'dataset': [], 'accuracy': [], 'transform': [], 'run': [], 'embedding': []}
#
#        for embedding_type in ['tsfresh', 'rocket']:
#            for run in range(10):
#                X_train_, X_test_ = interpolate_scale(X_train_, X_test_)
#                embedding = feature_factory(embedding_type, run)
#
#                X_train_rep = embedding.fit_transform(X_train_)
#                X_test_rep = embedding.transform(X_test_)
#
#                transform_levels = [0, 1e-17, 0.0000001, 0.000001, 0.00001, 0.0001, 0.0002, 0.0005, 0.001, 0.002, 0.005, 0.01, 0.02, 0.05, 0.1, 1]
#                for level in transform_levels:
#
#                    transform_string = f"{transform};{level}"
#                    transformation = transformation_factory(transform_string)
#
#                    X_train_transformed = transformation(X_train_.copy())
#                    X_test_transformed = transformation(X_test_.copy())
#
#                    X_train_rep_transform = embedding.transform(X_train_transformed)
#                    X_test_rep_transform = embedding.transform(X_test_transformed)
#
#                    clf = RandomForestClassifier(n_jobs=-1)
#                    clf.fit(pd.concat([X_train_rep, X_train_rep_transform], axis=0).reset_index(drop=True), ['o']*len(X_train_rep)+['t']*len(X_train_rep_transform))
#                    y_pred = clf.predict(pd.concat([X_test_rep, X_test_rep_transform], axis=0).reset_index(drop=True))
#
#                    accuracy = accuracy_score(['o']*len(X_test_rep)+['t']*len(X_test_rep_transform), y_pred)
#
#                    data['dataset'].append(dataset)
#                    data['accuracy'].append(accuracy)
#                    data['transform'].append(transform_string)
#                    data['run'].append(run)
#                    data['embedding'].append(embedding_type)
#                
#
#        folder = 'transformation_differencing'
#        create_directory_if_not_exist(folder)
#        file = f'{folder}/{dataset}.csv'
#        pd.DataFrame(data).to_csv(file, index=False)