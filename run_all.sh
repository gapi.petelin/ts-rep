#!/bin/bash

add_to_tsp() {
    local cmd="$1"
    # Add the command to the queue
    tsp $cmd
}

for SEED in 1 0
do
    for DATASET in "BirdChicken" "ChlorineConcentration" "AllGestureWiimoteZ" "ElectricDevices" "ShapesAll" "UWaveGestureLibraryY" "MixedShapesRegularTrain" "Lightning7" "ShakeGestureWiimoteZ" "CricketY" "FacesUCR" "UWaveGestureLibraryAll" "ECG200" "GunPointOldVersusYoung" "BME" "BeetleFly" "Car" "EOGHorizontalSignal" "ShapeletSim" "Coffee" "Haptics" "SonyAIBORobotSurface1" "EOGVerticalSignal" "SemgHandSubjectCh2" "GestureMidAirD2" "ArrowHead" "CinCECGTorso" "Adiac" "Trace" "HandOutlines" "Rock" "PowerCons" "InsectWingbeatSound" "PigAirwayPressure" "Herring" "FreezerSmallTrain" "CricketZ" "GesturePebbleZ1" "PhalangesOutlinesCorrect" "GestureMidAirD1" "CricketX" "OliveOil" "EthanolLevel" "WordSynonyms" "Wafer" "SmoothSubspace" "ItalyPowerDemand" "InsectEPGRegularTrain" "InsectEPGSmallTrain" "Mallat" "PickupGestureWiimoteZ" "GunPointAgeSpan" "Chinatown" "FordB" "SmallKitchenAppliances" "SemgHandGenderCh2" "Meat" "NonInvasiveFetalECGThorax1" "MiddlePhalanxOutlineCorrect" "FaceAll" "StarLightCurves" "AllGestureWiimoteY" "AllGestureWiimoteX" "DodgerLoopGame" "LargeKitchenAppliances" "PLAID" "PigCVP" "TwoPatterns" "TwoLeadECG" "Crop" "MoteStrain" "Earthquakes" "MelbournePedestrian" "DodgerLoopDay" "PigArtPressure" "Fish" "Wine" "SemgHandMovementCh2" "GunPointMaleVersusFemale" "DistalPhalanxOutlineAgeGroup" "FordA" "Beef" "Worms" "UWaveGestureLibraryZ" "FiftyWords" "OSULeaf" "MiddlePhalanxOutlineAgeGroup" "Phoneme" "Lightning2" "ProximalPhalanxOutlineCorrect" "UMD" "WormsTwoClass" "Plane" "Yoga" "MiddlePhalanxTW" "FaceFour" "ProximalPhalanxOutlineAgeGroup" "GesturePebbleZ2" "DistalPhalanxOutlineCorrect" "GestureMidAirD3" "Computers" "ProximalPhalanxTW" "InlineSkate" "MixedShapesSmallTrain" "DiatomSizeReduction" "Symbols" "DodgerLoopWeekend" "ToeSegmentation1" "FreezerRegularTrain" "ECGFiveDays" "GunPoint" "UWaveGestureLibraryX" "SonyAIBORobotSurface2" "NonInvasiveFetalECGThorax2" "ScreenType" "DistalPhalanxTW" "ToeSegmentation2" "MedicalImages" "Ham" "CBF" "Strawberry" "SwedishLeaf" "HouseTwenty" "SyntheticControl" "ECG5000" "ACSF1" "RefrigerationDevices" "Fungi"
    do
        for METHOD in "tsfresh" "rocket" "baseline" "catch22" "tsforest" "drcif" "weasel" "eknn" # "hivecotev2" "inception" "cnn"
        do
            add_to_tsp "python transformation_robustness_multi_transform.py --dataset $DATASET --method $METHOD --seed $SEED --transform None TimeWarp;0.01 TimeWarp;0.02 TimeWarp;0.05 TimeWarp;0.1 TimeWarp;0.2 TimeWarp;0.5 TimeWarp;1.0 Smooth;1 Smooth;3 Smooth;5 Smooth;7 Smooth;11 Smooth;21 Smooth;31 Smooth;41 Smooth;51 Smooth;71 Smooth;91 WhiteNoise;0 WhiteNoise;0.00001 WhiteNoise;0.0001 WhiteNoise;0.001 WhiteNoise;0.002 WhiteNoise;0.005 WhiteNoise;0.01 WhiteNoise;0.02 WhiteNoise;0.05 WhiteNoise;0.1 WhiteNoise;0.2 WhiteNoise;0.5 WhiteNoise;1.0 WhiteNoise;10.0 WhiteNoise;100.0 WhiteNoise;1000.0 WhiteNoise;10000.0 MagnitudeWarp;0.0 MagnitudeWarp;0.1 MagnitudeWarp;0.5 MagnitudeWarp;1.0 MagnitudeWarp;5.0 MagnitudeWarp;10.0 MagnitudeWarp;50.0 MagnitudeWarp;100.0 MagnitudeWarp;500.0 Permutation;1 Permutation;2 Permutation;3 Permutation;4 Permutation;5 Permutation;10 Permutation;20 WindowSlice;0.1 WindowSlice;0.5 WindowSlice;0.7 WindowSlice;0.9 WindowSlice;0.95 WindowSlice;1.0 Shift;0 Shift;1 Shift;2 Shift;3 Shift;4 Shift;5 Shift;6 Shift;7 Shift;8 Shift;9 Shift;10 Shift;50 Constant;0.0 Constant;0.1 Constant;1.0 Constant;10.0 Constant;100.0 Constant;1000.0 Spike;0 Spike;1 Spike;2 Spike;3 Spike;4 Spike;5 Spike;6 Spike;7 Spike;8 Spike;9 Spike;10 Spike;12 Spike;14 Spike;16 Spike;18 Spike;20 Spike;25 Spike;30 Spike;35 Spike;40 Spike;50 Spike;100 Spike;200 Spike;500 Spike;1000 Scale;5.0 Scale;2.0 Scale;1.0 Scale;0.75 Scale;0.5 Scale;0.25 Scale;0.0 Scale;-0.25 Scale;-0.5 Scale;-0.75 Scale;-1.0 Scale;-2.0 Scale;-5.0 Quantize;2 Quantize;4 Quantize;8 Quantize;16 Quantize;32 Quantize;64 DataLoss;10 DataLoss;50 DataLoss;70 DataLoss;90 DataLoss;95 DataLoss;97 DataLoss;97 RandomWalk;0 RandomWalk;0.00001 RandomWalk;0.0001 RandomWalk;0.001 RandomWalk;0.002 RandomWalk;0.005 RandomWalk;0.01 RandomWalk;0.02 RandomWalk;0.05 RandomWalk;0.1 RandomWalk;0.2 RandomWalk;0.5 RandomWalk;1.0 RandomWalk;2.0 RandomWalk;5.0"
        done
    done

done

exit 0

#for SEED in 0 1
#do
#    for TRANSFORM in "WhiteNoise;1.0" "RandomWalk;0.02" "Spike;3"
#    do
#        for DATASET in "Car" "ChlorineConcentration" "WordSynonyms" "SmoothSubspace" "Plane"
#        do
#            for METHOD in "rocket" "tsfresh" "catch22"
#            do
#                tsp python transformation_feature_importance.py --dataset $DATASET --method $METHOD --transform $TRANSFORM --seed $SEED
#            done
#        done
#    done
#done
#
#exit 0
#
#for SEED in 0 1 2
#do
#    for TRANSFORM in "WhiteNoise;0" "WhiteNoise;0.00001" "WhiteNoise;0.0001" "WhiteNoise;0.001" "WhiteNoise;0.01" "WhiteNoise;0.1" "WhiteNoise;1.0" "WhiteNoise;10.0" "RandomWalk;0" "RandomWalk;0.0001" "RandomWalk;0.001" "RandomWalk;0.01" "RandomWalk;0.02" "RandomWalk;0.05" "RandomWalk;0.1" 
#    do
#        for DATASET in "Car" "ChlorineConcentration" "WordSynonyms" "SmoothSubspace" "Plane"
#        do
#            for METHOD in "rocket" "catch22" "tsforest" "cnn" "tsfresh"
#            do
#                tsp python transformation_differencing.py --dataset $DATASET --method $METHOD --transform $TRANSFORM --seed $SEED
#            done
#        done
#    done
#done



