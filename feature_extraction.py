import argparse
from sktime.transformations.panel.tsfresh import TSFreshFeatureExtractor, TSFreshRelevantFeatureExtractor
from sktime.datasets import load_from_ucr_tsv_to_dataframe
import os
from utils import *
from sktime.transformations.panel.rocket import Rocket, MiniRocket
from sktime.transformations.series.impute import Imputer

def main(feature_type, dataset, seed, tranform):
    print(f"feature type: {feature_type}, dataset: {dataset}")
    
    save_dir = f'representation/{dataset}/{feature_type}_{tranform}_{seed}'
    train_file = f'{save_dir}/train.parquet'
    test_file = f'{save_dir}/test.parquet'
    
    if os.path.exists(train_file) and os.path.exists(test_file):
        print(f'Files {train_file} and {test_file} already exist.')
    else:
        print(f'Computing files {train_file} and {test_file}.')

        X_train, y_train = load_from_ucr_tsv_to_dataframe(
            os.path.join('UCRArchive_2018/', f"{dataset}/{dataset}_TRAIN.tsv")
        )
        
        X_test, y_test = load_from_ucr_tsv_to_dataframe(
            os.path.join('UCRArchive_2018/', f"{dataset}/{dataset}_TEST.tsv")
        )
        
        X_train, X_test = interpolate_scale(X_train, X_test)
        
        embedding = feature_factory(feature_type, seed)
        embedding.fit(X_train, y_train)
        
        transformation = transformation_factory(tranform)
        X_test = transformation(X_test)
        
        X_train_rep = embedding.transform(X_train)
        X_test_rep = embedding.transform(X_test)

        create_directory_if_not_exist(save_dir)

        X_train_rep.columns = [str(x) for x in X_train_rep.columns]
        X_test_rep.columns = [str(x) for x in X_test_rep.columns]

        X_train_rep.to_parquet(train_file)
        X_test_rep.to_parquet(test_file)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--feature_type", required=True)
    parser.add_argument("--dataset", required=True)
    parser.add_argument("--seed", required=True)
    parser.add_argument("--transform", required=True)
    args = parser.parse_args()
    main(args.feature_type, args.dataset, args.seed, args.transform)