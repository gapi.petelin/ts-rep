from pathlib import Path
from functools import partial
import numpy as np
import pandas as pd
import os
from sktime.transformations.series.adapt import TabularToSeriesAdaptor
from sklearn.preprocessing import StandardScaler
from sktime.transformations.panel.tsfresh import TSFreshFeatureExtractor, TSFreshRelevantFeatureExtractor
from sktime.transformations.panel.rocket import Rocket, MiniRocket
from sktime.transformations.series.impute import Imputer
from sktime.datasets import load_from_ucr_tsv_to_dataframe
import glob
from sktime.classification.hybrid._hivecote_v2 import HIVECOTEV2
from sktime.classification.dictionary_based import WEASEL
import time
from joblib import Parallel, delayed


def trained_classifiers_factory(dataset, method, seed, save_folder='~/data/trained_classifiers_factory'):
    save_folder = os.path.expanduser(save_folder)
    import joblib
    create_directory_if_not_exist(save_folder)
    file = f'{dataset}__{method}__{seed}.joblib'
    model_path = os.path.join(save_folder, file)
    if os.path.exists(model_path):
        start_time = time.time()
        clf = joblib.load(model_path)
        end_time = time.time()
        elapsed_time = end_time - start_time
        print('Loading model time', elapsed_time)
    else:
        start_time = time.time()
        X_train, _, y_train, _ = read_dataset(dataset)
        X_train = interpolate_scale_single(X_train)
        clf = classifiers_factory(method, seed)
        clf.fit(X_train, y_train)
        if method not in ['cnn', 'inception']:
            joblib.dump(clf, model_path)
        end_time = time.time()
        elapsed_time = end_time - start_time
        print('Training model time', elapsed_time)
    return clf
    
    #X_train, _, y_train, _ = read_dataset(dataset)
    #X_train = interpolate_scale_single(X_train)
    #clf = classifiers_factory(method, seed)
    #clf.fit(X_train, y_train)
    #return clf

def classifiers_factory(model, seed):
    from sktime.transformations.panel.rocket import Rocket
    from sklearn.ensemble import RandomForestClassifier
    from sklearn.pipeline import Pipeline
    from sktime.classification.interval_based import TimeSeriesForestClassifier
    from sktime.transformations.panel.catch22 import Catch22
    from sktime.classification.deep_learning.cnn import CNNClassifier
    from sktime.classification.deep_learning.tapnet import TapNetClassifier
    from sktime.classification.distance_based import ProximityForest
    from sktime.transformations.panel.tsfresh import TSFreshFeatureExtractor
    from sktime.classification.distance_based import KNeighborsTimeSeriesClassifier
    from sktime.classification.feature_based import MatrixProfileClassifier
    from sktime.classification.interval_based import DrCIF
    from sktime.classification.dictionary_based import TemporalDictionaryEnsemble
    from sktime.classification.deep_learning.inceptiontime import InceptionTimeClassifier
    from sktime.classification.dummy import DummyClassifier
    
    if model == 'rocket':
        return Pipeline([
            ("rocket", Rocket(n_jobs=-1, random_state=seed)),
            ("classifier", RandomForestClassifier(n_jobs=-1, random_state=seed))
        ])
    elif model == 'catch22':
        return Pipeline([
            ("catchh22", Catch22(n_jobs=-1, replace_nans=True)),
            ("classifier", RandomForestClassifier(n_jobs=-1, random_state=seed))
        ])
    elif model == 'tsfresh':
        return Pipeline([
            ("tsfresh", TSFreshFeatureExtractor(n_jobs=-1, default_fc_parameters="efficient", show_warnings=False)),
            ("classifier", RandomForestClassifier(n_jobs=-1, random_state=seed))
        ])
    elif model == 'tsforest':
        return TimeSeriesForestClassifier(n_jobs=-1, random_state=seed)
    elif model == 'cnn':
        return CNNClassifier(random_state=seed)
    elif model == 'tapnet':
        return TapNetClassifier(random_state=seed)
    elif model == 'hivecotev2':
        return HIVECOTEV2(random_state=seed, n_jobs=-1, time_limit_in_minutes=2)
    elif model== 'proxforest':
        return ProximityForest(random_state=seed)
    elif model=='weasel':
        return WEASEL(random_state=seed)
    elif model=='knn':
        return KNeighborsTimeSeriesClassifier(distance="dtw", n_jobs=-1)
    elif model=='eknn':
        return KNeighborsTimeSeriesClassifier(distance="euclidean", n_jobs=-1)
    elif model=='matrixprofile':
        return MatrixProfileClassifier(n_jobs=-1, random_state=seed)
    elif model=='drcif':
        return DrCIF(n_jobs=-1, random_state=seed)
    elif model=='tde':
        return TemporalDictionaryEnsemble(n_jobs=-1, random_state=seed)
    elif model=='inception':
        return InceptionTimeClassifier(random_state=seed)
    elif model=='baseline':
        return DummyClassifier(random_state=seed)
    else:
        raise ValueError(f'Model {model} not in the list')

def generate_random_walk(n_steps, std_dev):
    return np.random.normal(0, std_dev, n_steps).cumsum()

def random_walk(df, level):
    l = []
    X_test_ = pd.DataFrame()
    for i in range(len(df)):
        series = df['dim_0'].iloc[i]
        l.append(pd.Series(series+generate_random_walk(len(series), level)))
    X_test_["dim_0"] = l
    return X_test_

def white_noise(df, level):
    l = []
    X_test_ = pd.DataFrame()
    for i in range(len(df)):
        series = df['dim_0'].iloc[i]
        l.append(pd.Series(series + np.random.normal(0, level, len(series))))
    X_test_["dim_0"] = l
    return X_test_

def mul_constant(df, level):
    l = []
    X_test_ = pd.DataFrame()
    for i in range(len(df)):
        series = df['dim_0'].iloc[i]
        l.append(pd.Series(series*level))
    X_test_["dim_0"] = l
    return X_test_

def resample(df, level):
    l = []
    X_test_ = pd.DataFrame()
    from sktime.transformations.panel.interpolate import TSInterpolator
    for i in range(len(df)):
        series = df['dim_0'].iloc[i]
        tf = TSInterpolator(int(len(series)*level))
        l.append(tf.fit_transform(series))
    X_test_["dim_0"] = l
    return X_test_

def add_constant(df, level):
    l = []
    X_test_ = pd.DataFrame()
    for i in range(len(df)):
        series = df['dim_0'].iloc[i]
        l.append(pd.Series(series + np.random.normal(0, level)))
    X_test_["dim_0"] = l
    return X_test_

def add_spike(df, level):
    l = []
    X_test_ = pd.DataFrame()
    for i in range(len(df)):
        series = df['dim_0'].iloc[i].copy()
        std_dev = series.std()
        for _ in range(level):
            random_idx = np.random.choice(series.index)
            direction = np.random.choice([-1, 1])
            series[random_idx] += 3*std_dev*direction
        l.append(series)
    X_test_["dim_0"] = l
    return X_test_

def add_shift(df, level):
    l = []
    X_test_ = pd.DataFrame()
    for i in range(len(df)):
        series = df['dim_0'].iloc[i].copy()
        std_dev = series.std()
        for _ in range(level):
            random_idx = np.random.choice(series.index)
            direction = np.random.choice([-1, 1])
            series[random_idx:] += std_dev*direction
        l.append(series)
    X_test_["dim_0"] = l
    return X_test_

def none_transform(df):
    return df

def window_slice(df, reduce_ratio):
    if reduce_ratio == 1.0:
        return df
    l = []
    X_test_ = pd.DataFrame()
    for i in range(len(df)):
        series = df['dim_0'].iloc[i]
        l.append(window_slice_v(list(series), reduce_ratio=reduce_ratio))
    X_test_["dim_0"] = l
    return X_test_

def window_slice_v(x, reduce_ratio):
    x = np.array(x).astype(float).reshape(1, len(x), 1)
    # https://halshs.archives-ouvertes.fr/halshs-01357973/document
    target_len = np.ceil(reduce_ratio*x.shape[1]).astype(int)
    if target_len >= x.shape[1]:
        return x
    starts = np.random.randint(low=0, high=x.shape[1]-target_len, size=(x.shape[0])).astype(int)
    ends = (target_len + starts).astype(int)
    
    ret = np.zeros_like(x)
    for i, pat in enumerate(x):
        for dim in range(x.shape[2]):
            ret[i,:,dim] = np.interp(np.linspace(0, target_len, num=x.shape[1]), np.arange(target_len), pat[starts[i]:ends[i],dim]).T
    return pd.Series(ret.flatten())

def permutation(df, max_segments):
    l = []
    X_test_ = pd.DataFrame()
    for i in range(len(df)):
        series = df['dim_0'].iloc[i]
        if max_segments < 2:
            series_per = series
        else:
            series_per = permutation_v(list(series), max_segments=max_segments)
        l.append(series_per)
    X_test_["dim_0"] = l
    return X_test_

def permutation_v(x, max_segments=5, seg_mode="equal"):
    x = np.array(x).astype(float).reshape(1, len(x), 1)
    orig_steps = np.arange(x.shape[1])
    
    #num_segs = np.random.randint(1, max_segments, size=(x.shape[0]))
    num_segs = np.random.randint(max_segments, max_segments+1, size=(x.shape[0]))
    #print(np.random.randint(1, max_segments, size=(x.shape[0])))
    #num_segs = max_segments
    #print(num_segs)
    
    ret = np.zeros_like(x)
    for i, pat in enumerate(x):
        if num_segs[i] > 1:
            while True:
                if seg_mode == "random":
                    split_points = np.random.choice(x.shape[1]-2, num_segs[i]-1, replace=False)
                    split_points.sort()
                    splits = np.split(orig_steps, split_points)
                else:
                    splits = np.array_split(orig_steps, num_segs[i])
                warp = np.concatenate(np.random.permutation(splits)).ravel()
                not_at_position = np.where(warp != np.arange(len(warp)))[0]
                if len(not_at_position) == len(warp):
                    break
            ret[i] = pat[warp]
        else:
            ret[i] = pat
    return pd.Series(ret.flatten())

def magnitude_warp(df, sigma):
    l = []
    X_test_ = pd.DataFrame()
    for i in range(len(df)):
        series = df['dim_0'].iloc[i]
        l.append(magnitude_warp_v(list(series), sigma=sigma))
    X_test_["dim_0"] = l
    return X_test_

def magnitude_warp_v(x, sigma=0.2, knot=6):
    x = np.array(x).astype(float).reshape(1, len(x), 1)
    from scipy.interpolate import CubicSpline
    orig_steps = np.arange(x.shape[1])
    
    random_warps = np.random.normal(loc=1.0, scale=sigma, size=(x.shape[0], knot+2, x.shape[2]))
    warp_steps = (np.ones((x.shape[2],1))*(np.linspace(0, x.shape[1]-1., num=knot+2))).T
    ret = np.zeros_like(x)
    for i, pat in enumerate(x):
        warper = np.array([CubicSpline(warp_steps[:,dim], random_warps[i,:,dim])(orig_steps) for dim in range(x.shape[2])]).T
        ret[i] = pat * warper
    return pd.Series(ret.flatten())

def drop_random_values(array, percentage):
    array = array.copy()
    if percentage < 0 or percentage > 100:
        raise ValueError("Percentage must be between 0 and 100")

    total_values = array.size
    num_values_to_drop = int(total_values * percentage / 100)
    indices_to_drop = np.random.choice(total_values, num_values_to_drop, replace=False)
    np.put(array, indices_to_drop, np.nan)
    return array

def data_loss(df, level):
    l = []
    X_test_ = pd.DataFrame()
    for i in range(len(df)):
        imp = Imputer(method='linear')
        series = df['dim_0'].iloc[i]
        series_d = drop_random_values(np.array(series), level)
        series_di = imp.fit_transform(series_d).flatten()
        l.append(pd.Series(series_di))
    X_test_["dim_0"] = l
    return X_test_

def quantize_values(values, n_levels):
    if n_levels <= 0:
        raise ValueError("Number of levels must be greater than 0")
    # Define the quantization levels
    min_val, max_val = min(values), max(values)
    levels = np.linspace(min_val, max_val, n_levels)
    # Quantize each value
    quantized_values = [min(levels, key=lambda x: abs(x - value)) for value in values]
    return np.array(quantized_values)

def quantize(df, level):
    l = []
    X_test_ = pd.DataFrame()
    for i in range(len(df)):
        series = df['dim_0'].iloc[i]
        seriesq = quantize_values(np.array(series), level)
        l.append(pd.Series(seriesq))
    X_test_["dim_0"] = l
    return X_test_

def transformation_factory(transformation_type):
    transformation, *params = transformation_type.split(';')
    if transformation=='WhiteNoise':
        return partial(white_noise, level=float(params[0]))
    elif transformation=='Constant': # TODO think about adding a random constant
        return partial(add_constant, level=float(params[0]))
    elif transformation=='RandomWalk':
        return partial(random_walk, level=float(params[0]))
    elif transformation=='Scale':
        return partial(mul_constant, level=float(params[0]))
    elif transformation=='Resample':
        return partial(resample, level=float(params[0]))
    elif transformation=='Spike':
        return partial(add_spike, level=int(params[0]))
    elif transformation=='Shift':
        return partial(add_shift, level=int(params[0]))
    elif transformation=='WindowSlice':
        return partial(window_slice, reduce_ratio=float(params[0]))
    elif transformation=='Permutation':
        return partial(permutation, max_segments=int(params[0]))
    elif transformation=='MagnitudeWarp':
        return partial(magnitude_warp, sigma=float(params[0]))
    elif transformation=='DataLoss':
        return partial(data_loss, level=float(params[0]))
    elif transformation=='Quantize':
        return partial(quantize, level=int(params[0]))
    elif transformation=='None':
        return none_transform
    else:
        return None
    
def interpolate_scale_single(X):
    scaler = Imputer(method='linear') * TabularToSeriesAdaptor(StandardScaler())
    return scaler.fit_transform(X)
    
def interpolate_scale(X_train, X_test):
    return interpolate_scale_single(X_train), interpolate_scale_single(X_test)

def feature_factory(feature_type, seed):
    from sktime.transformations.panel.catch22 import Catch22
    feature_types = {
        'tsfresh': TSFreshFeatureExtractor(default_fc_parameters="efficient", show_warnings=False, n_jobs=-1, disable_progressbar=True),
        'rocket': Rocket(n_jobs=-1, random_state=seed),
        'minirocket': MiniRocket(n_jobs=-1, random_state=seed),
        'catch22': Catch22(n_jobs=-1, replace_nans=True)
    }
    return feature_types[feature_type]

def create_directory_if_not_exist(directory):
    Path(directory).mkdir(parents=True, exist_ok=True)
    
def read_dataset(dataset):
    X_train, y_train = load_from_ucr_tsv_to_dataframe(
        os.path.join('UCRArchive_2018/', f"{dataset}/{dataset}_TRAIN.tsv")
    )
    X_test, y_test = load_from_ucr_tsv_to_dataframe(
        os.path.join('UCRArchive_2018/', f"{dataset}/{dataset}_TEST.tsv")
    )
    return X_train, X_test, y_train, y_test

def get_all_datasets():
    return [x.split('/')[1] for x in glob.glob('UCRArchive_2018/*') if '_' not in x.split('/')[1]]

def filter_out_incomplete_datasets(df):
    all_datasets = set(df.dataset.unique())
    methods = df.method.unique()
    transforms = df['transform'].unique()
    print(methods)
    print(transforms)
    n_pairs = len(transforms) * len(methods)
    print(n_pairs)
    datasets_with_all_combinations = set(df.groupby('dataset').count().reset_index().query(f'method=={n_pairs}').dataset.unique())
    df = df[df['dataset'].isin(datasets_with_all_combinations)]
    print("not complete datasets:", all_datasets - datasets_with_all_combinations)
    return df

def load_robustnes(transform):
    files = glob.glob(f'robustness/*__*__{transform}*__*.csv')
    print(f'Loaded {len(files)} files')
    dfs = Parallel(n_jobs=-1)(delayed(lambda file: pd.read_csv(file, index_col=False))(file=file) for file in files)
    df = pd.concat(dfs)
    #df['level'] = df['transform'].apply(lambda x: float(x.split(';')[1]))
    #df['transform_type'] = df['transform'].apply(lambda x: x.split(';')[0])

    def extract_level(x):
        try:
            return float(x.split(';')[1])
        except:
            return None

    def extract_transform(x):
        try:
            return float(x.split(';')[0])
        except:
            return x

    # Apply the function to create the 'level' column
    df['level'] = df['transform'].apply(extract_level)
    df['transform_type'] = df['transform'].apply(extract_transform)
    
    return df

