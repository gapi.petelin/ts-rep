from sklearn.metrics import accuracy_score
from datetime import datetime
import argparse
import os

def define_groups(embedding, columns):
    if embedding == 'tsfresh':
        d = {}
        for column in columns:
            group = column.split('__')[1]
            if group not in d:
                d[group] = []
            d[group].append(column)
            
        l = []
        for k, v in d.items():
            l.append((k, v))
        return l
    else:
        l = []
        for v in columns:
            l.append((v, [v]))
        return l

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--dataset', type=str, required=True)
    parser.add_argument('--method', type=str, required=True)
    parser.add_argument('--transform', type=str, required=True)
    parser.add_argument('--seed', type=int, default=0)
    
    args = parser.parse_args()
    print(args)
    
    folder = 'feature_importance'
    file = f'{folder}/{args.dataset}__{args.method}__{args.transform}__{args.seed}.csv'
    if os.path.exists(file):
        print(f'Files {file} already exist.')
    else:
        dffi = {'feature_name': [], 'feature_accuracy': [], 'run': [], 'transform': [], 'dataset': [], 'method':[]}
        print('Computing...')
        from utils import *
        from sklearn.model_selection import train_test_split
        from sklearn.ensemble import RandomForestClassifier
        from sklearn.metrics import accuracy_score, classification_report

        X_train, X_test, y_train, y_test = read_dataset(args.dataset)
        X_train, X_test = interpolate_scale(X_train, X_test)

        X_original = pd.concat([X_train, X_test], axis=0).reset_index(drop=True)
        X_transformed = X_original.copy()

        # Always use the same seed
        embedding = feature_factory(args.method, 0)
        X_original_rep = embedding.fit_transform(X_original)
        transformation = transformation_factory(args.transform)
        X_transformed = transformation(X_transformed)
        X_transformed_rep = embedding.transform(X_transformed)

        for feature_group, features in define_groups(args.method, list(X_original_rep.columns)):
            print(features)
            X = pd.concat([X_original_rep, X_transformed_rep], axis=0).reset_index(drop=True)
            y = np.array([0]*len(X_original_rep) + [1]*len(X_transformed_rep))
            
            X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1)
            
            clf = RandomForestClassifier(n_jobs=-1, random_state=args.seed)
            clf.fit(X_train[features], y_train)
            
            y_pred = clf.predict(X_test[features])
            
            accuracy = accuracy_score(y_test, y_pred)
            
            dffi['feature_name'].append(feature_group)
            dffi['feature_accuracy'].append(accuracy)
            dffi['run'].append(args.seed)    
            dffi['transform'].append(args.transform)   
            dffi['dataset'].append(args.dataset)
            dffi['method'].append(args.method)

        print('Creating file')
        create_directory_if_not_exist(folder)
        df = pd.DataFrame(dffi).to_csv(file, index=False)

#        X_train, X_test = X_train.sample(n=size, replace=True), X_test.sample(n=size, replace=True)
#        
#        transformation = transformation_factory(args.transform)
#        X_train_transformed = transformation(X_train.copy())
#        X_test_transformed = transformation(X_test.copy())
#        
#        clf = classifiers_factory(args.method, args.seed)
#        clf.fit(pd.concat([X_train, X_train_transformed], axis=0).reset_index(drop=True), np.array(['o']*len(X_train)+['t']*len(X_train_transformed)))
#        y_pred = clf.predict(pd.concat([X_test, X_test_transformed], axis=0).reset_index(drop=True))
#        acc = accuracy_score(['o']*len(X_test)+['t']*len(X_test_transformed), y_pred)
#        
#        data = {'dataset': [], 'method': [], 'transform': [], 'seed':[], 'accuracy':[]}
#        data['dataset'].append(args.dataset)
#        data['method'].append(args.method)        
#        data['transform'].append(args.transform)
#        data['seed'].append(args.seed)
#        data['accuracy'].append(acc)    
#        create_directory_if_not_exist(folder)
#        df = pd.DataFrame(data).to_csv(file, index=False)
